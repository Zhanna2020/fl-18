import {GET_AUTHORS, ADD_AUTHORS, FIND_AUTHOR} from './actionTypes';

export const getAuthorsAction = (payload) => ({type: GET_AUTHORS, payload});

export const addAuthorsAction = (payload) => ({type: ADD_AUTHORS, payload});

export const findAuthorAction = (payload) => ({type: FIND_AUTHOR, payload});