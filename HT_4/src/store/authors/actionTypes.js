const GET_AUTHORS = 'GET_AUTHORS';

const ADD_AUTHORS = 'ADD_AUTHORS';

const FIND_AUTHOR = 'FIND_AUTHOR';

export {GET_AUTHORS, ADD_AUTHORS, FIND_AUTHOR};