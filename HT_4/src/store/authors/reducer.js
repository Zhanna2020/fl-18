import { toBePartiallyChecked } from '@testing-library/jest-dom/dist/matchers';
import {GET_AUTHORS, ADD_AUTHORS, FIND_AUTHOR} from './actionTypes';

const authorsInitialState = {
    authors: [],
}

export const authorsReducer = (state = authorsInitialState, action) =>  {
    switch (action.type) {
        case GET_AUTHORS:
            return {...state, authors: [...action.payload]};
        case ADD_AUTHORS:
            return {...state, authors: [...state.authors, action.payload]};
        case FIND_AUTHOR:
            const id = state.authors.filter((item) => {
                if(item.name === action.payload.name) {
                    return item.id;
                }
                return 0;
            });
            console.log(id);
            return {...state, id};
        default:
            return state;
    }
};