import {createStore , combineReducers, applyMiddleware} from 'redux';
import {coursesReducer} from './courses/reducer';
import {authorsReducer} from './authors/reducer';
import {userReducer} from './user/reducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    courses: coursesReducer,
    authors: authorsReducer,
    user: userReducer,
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));