import React from 'react';

import Button from "../../common/Button/Button";
import Logo from "./components/Logo";

import './Header.css';

import {buttonTextHeader} from '../../constants'

function Header() {
  return (
      <header id = 'header'>
          <div className="header__wrapper">
           <div className="logo__wrapper">
               <a href="#header"><Logo className = "logo"/></a>
           </div>
           <div className="user__block">
               <p className="user__name">Dave</p>
               <Button buttonText = {buttonTextHeader}/>
           </div>
          </div>          
      </header>
    );
}
export default Header;
