import Button from "../../common/Button/Button";
import Input from "../../common/Input/Input";

import { v4 as uuidv4 } from 'uuid';


import {enterAuthor, 
    authorName, 
    addAuthor, 
    enterDuration, 
    duration, 
    deleteAuthor, 
    createAuthor, 
    createCourse, 
    title, 
    enterTitle, 
    minInHour,
    exit} from '../../constants';

import './CreateCourse.css';

import React, {useState, useEffect, useRef} from "react";

function CreateCourse(props) {
    const [newtitle, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [newduration, setDuration] = useState(0);
    const [newAuthor, setnewAuthor] = useState('');
    const [request, setRequest] = useState();
    const [requestCourse, setrequestCourse] = useState();


    const [formatduration, setformatDuration] = useState(0);

    const [handleErrors, sethandleErrors] = useState(false);

    const [dirtytitle, setdirtyTitle] = useState(false);
    const [dirtydescription, setdirtyDescription] = useState(false);
    const [dirtynewduration, setdirtyDuration] = useState(false);
    const [newdirtyAuthor, setdirtynewAuthor] = useState(false);

    const [arrAuthors, setarrAuthors] = useState([]);
    const [authorsView, setauthorsView] = useState();


    const [authorsViewCourse, setauthorsViewCourse] = useState();
    const [arrAuthorsCourse, setarrAuthorscourse] = useState([]);



    const [dirtytitleError, setdirtyTitleError] = useState('Input can not be empty');
    const [dirtydescriptionError, setdirtyDescriptionError] = useState('Input can not be empty');
    const [dirtynewdurationError, setdirtyDurationError] = useState('Input can not be empty');
    const [newdirtyAuthorError, setdirtynewAuthorError] = useState('Input can not be empty');

   

    const authorsAdd = useRef([])

    const authors = useRef([]);
    const authorsCourse = useRef([]);



    const author = {
        id: uuidv4(),
        name: newAuthor
    }

    const course = {
    id: uuidv4(),
    title: newtitle,
    description: description,
    creationDate: formatDate(new Date()),
    duration: parseInt(newduration),
    authors: getId(arrAuthorsCourse)
   }
   


   function getId(obj) {
    let arrId = obj.map((el) => {
        return el.id;
    })
    return arrId;
   }
   function formatDate(date) {

    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;
  
    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;
  
    var yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;
  
    return dd + '/' + mm + '/' + yy;
  }
  
   

    useEffect(() => {
        const rezAdd = arrAuthors.map((item) =>
        <div key={uuidv4()} className="added__author">
        <p className="added__name">{item.name}</p>
        <Button id={item.id} buttonText = {addAuthor} 
        onClick = {addToCourse}/>
        </div>);
        setauthorsView(rezAdd);
    },[request, arrAuthors])

    useEffect(() => {
        const rezAddCourse = arrAuthorsCourse.map((item) =>
        <div key={uuidv4()} className="added__author">
        <p className="added__name">{item.name}</p>
        <Button id = {item.id} buttonText = {deleteAuthor} 
        onClick = {deleteFromCourse}/> </div>);
        setauthorsViewCourse(rezAddCourse);
        console.log('view')
    },[requestCourse, arrAuthorsCourse])

    function deleteFromCourse(e) {
        e.preventDefault();
        let id = e.target.id;
        let newCourseAuthors = [];
        authorsCourse.current.filter((item) => {
            if(item.id !== id) {
                newCourseAuthors.push(item);
                return item;
            }
            return 0;
        })
        authorsCourse.current = newCourseAuthors;
        setarrAuthorscourse(authorsCourse.current);
        setrequestCourse(uuidv4());
    }

    function addToCourse(e) {
        e.preventDefault();
        let id = e.target.id;
        let courseAuthor = {};
        authors.current.filter((item) => {
            if(item.id === id) {
                courseAuthor = item;
                return item;
            }
            return 0;
        })
        authorsCourse.current.push(courseAuthor);
        setarrAuthorscourse(authorsCourse.current);
        setrequestCourse(uuidv4());
    }


    function addAuthorClick(e) {
        e.preventDefault();
        if(newdirtyAuthorError === '') {
            authors.current.push(author);
            authorsAdd.current.push(author)
            props.updateAuthor(author)
            console.log(author);
            setarrAuthors(authors.current);
            setRequest(uuidv4());
            setnewAuthor('');
        }
    }

    function durationFormat(minutes) {
        if(minutes !== null && minutes.length !== 0 && minutes !== 0) {
            let hours = (minutes / minInHour);
            let rhours = Math.floor(hours).toString();
            let min = (hours - rhours) * minInHour;
            let rminutes = Math.round(min).toString();
            let rezHour = '';
            let rezMin = '';
            if(rhours.length === 1) {
                rezHour = '0' + rhours;
            } else {
                rezHour = rhours;
            }
            if(rminutes.length === 1) {
                rezMin = '0' + rminutes;
            } else {
                rezMin = rminutes;
            }
            
            return rezHour + ':' + rezMin +  ' hours';
        } 
        return 0;
    }

    const validation = (e) => {
        switch (e.target.name) {
            case 'title':
                if(newtitle.length <= 2) {
                    setdirtyTitleError('Title should be more then 2 characters');
                    setdirtyTitle(true);
                } else {
                    setdirtyTitleError('');
                    setdirtyTitle(false);
                }
                break;
            case 'description':
                if(description.length <= 2) {
                    setdirtyDescriptionError('Description should be more then 2 characters');
                    setdirtyDescription(true);
                } else {
                    setdirtyDescriptionError('');
                    setdirtyDescription(false);
                }
                break;
            case 'duration':
                if(newduration <= 0) {
                    setdirtyDurationError('Duration should be more then 0');
                    setdirtyDuration(true);
                } else {
                    setdirtyDurationError('');
                    setdirtyDuration(false);
                }
                break;
            case 'newAuthor':
                if(newAuthor.length <= 2) {
                    setdirtynewAuthorError('Author should be more then 2 characters');
                    setdirtynewAuthor(true);
                } else {
                    setdirtynewAuthorError('');
                    setdirtynewAuthor(false);
                }
                break;
            default:
                break;
    }
}


    const blurHandler = (e) => {
        switch (e.target.name) {
        case 'title':
            setdirtyTitle(true);
            break;
        case 'description':
            setdirtyDescription(true);
            break;
        case 'duration':
            setdirtyDuration(true);
            break;
        case 'newAuthor':
            setdirtynewAuthor(true);
            break;
        default:
            break;
        }

    }

    function handleClick(e) {
        e.preventDefault();
        if(dirtytitleError !== '') {
            sethandleErrors(false);
        } else if(dirtydescriptionError !== ''){
            sethandleErrors(false);
        } else if(dirtynewdurationError !== ''){
            sethandleErrors(false);
        } else if(arrAuthorsCourse.length === 0){
            sethandleErrors(false);
        } else {
            props.create(course);
            sethandleErrors(true);
        }
    }
        return (
            <>
        <form className = "add_course_box">
                {handleErrors &&     
                <React.Fragment>
               <div className="error">Course created!</div>    
               </React.Fragment>}
         <Button buttonText = {createCourse} 
                    onClick = {handleClick}
                    type = "submit"/>
         <Button buttonText = {exit}
                    onClick = {props.onClick}
                    type = "submit"/>
                <div className="title">
                {(dirtytitle && dirtytitleError) &&     
                <React.Fragment>
               <div className="error">{dirtytitleError}</div>    
               </React.Fragment>}
            <Input placeholdetText = {enterTitle} 
                    onChange = {(e) => {
                        setTitle(e.target.value);
                        validation(e);
                    }} 
                    labelText={title}
                    name = 'title'
                    onBlur = {e => blurHandler(e)}
                    value={newtitle}
                    type = 'text'/>
            
            </div>
            <div className="description__box">
                <label className="desc__title">Description</label>
                {(dirtydescription && dirtydescriptionError) &&     
                <React.Fragment>
               <div className="error">{dirtydescriptionError}</div>    
               </React.Fragment>}
                <textarea className="description" 
                onChange = {(e) => {
                    setDescription(e.target.value);
                    validation(e);
                }} 
                placeholder="Enter descriprion"
                name='description'
                onBlur = {e => blurHandler(e)}
                value={description}
                />
            </div>
            <div className="add_info_box">
                <div className="box">
                    <h3 className = "add__title">Add author</h3>
                    {(newdirtyAuthor && newdirtyAuthorError) &&     
                <React.Fragment>
               <div className="error">{newdirtyAuthorError}</div>    
               </React.Fragment>}
                    <Input placeholdetText = {enterAuthor} 
                    onChange = {(e) => {
                        setnewAuthor(e.target.value);
                        validation(e);
                    }} 
                    labelText={authorName}
                    name = 'newAuthor'
                    onBlur = {e => blurHandler(e)}
                    value={newAuthor}
                    type = 'text'/>
                     <Button buttonText = {createAuthor} 
                       onClick = {addAuthorClick}/>
                </div>
                <div className="box">
                    <h3 className = "add__title">Authors</h3>
                       {authorsView}
                </div>
                <div className="box">
                    <h3 className = "add__title">Duration</h3>
                    {(dirtynewduration && dirtynewdurationError) &&     
                <React.Fragment>
               <div className="error">{dirtynewdurationError}</div>    
               </React.Fragment>}
                    <Input placeholdetText = {enterDuration} 
                    onChange = {(e) => {
                        setDuration(e.target.value);
                        validation(e);
                        setformatDuration(durationFormat(e.target.value));
                    }} 
                    labelText={duration}
                    name = 'duration'
                    onBlur = {e => blurHandler(e)}
                    value={newduration}
                    type='number'/>
                    <div className="duration__rez">Duration: <span>{formatduration}</span></div>
                </div>
                <div className="box">
                    <h3 className = "add__title">Course authors</h3>
                    {authorsViewCourse}
                </div>
            </div>
        </form>
        </>
      );
  }
export default CreateCourse;

