import Button from "../../common/Button/Button";
import SearchBar from "./components/SearchBar/SearchBar";
import CourseCard from './components/CourseCard/CourseCard';
import CreateCourse from "../CreateCourse/CreateCourse";


import {mockedCoursesList, btnAddNew, title, mockedAuthorsList} from '../../constants';

import './Courses.css';

import React, {useState, useEffect} from "react";

function Courses() {
    const [showCourse, setshowCourse] = useState(true);
    const [showaddCourse, setshowaddCourse] = useState(false);

    const [searchTerm, setSearchTerm] = useState('');
    const [request, setRequest] = useState();
    const [result, setResult] = useState();

    const [courses, setCourses] = useState(mockedCoursesList);
    const [authors, setAuthors] = useState(mockedAuthorsList);

    const [updateRequest, setupdateRequest] = useState();

    
    useEffect(() => {
        const rez = courses.filter((obj) => {
            if(searchTerm === ''){
                return obj;
            } else if(obj.title.toLowerCase().includes(searchTerm.toLowerCase())){
                return obj;
            } else if(obj.id.toLowerCase().includes(searchTerm.toLowerCase())) {
                return obj;
            }
            return 0;
          }).map((obj) =>
          <CourseCard key={obj.id} 
          authorsList = {authors}
          title = {obj.title} 
          description = {obj.description} 
          authors = {obj.authors}
          duration = {obj.duration}
          creationDate = {obj.creationDate}/>);
        setResult(rez);
    },[searchTerm, request, updateRequest, courses, authors, showaddCourse])


    return (
        <div className = "user__section">
            {showCourse ?  <React.Fragment>
                <div className="user__section_row">
                <SearchBar 
                onChange = {(event) => {
                    setSearchTerm(event.target.value);
                }}
                onClick = {() => setRequest(searchTerm)}
                labelText = {title}/>
                <Button buttonText = {btnAddNew} 
                onClick = {() => {
                    setshowCourse(false);
                    setshowaddCourse(true);
                }}
                />
            </div>
            <div className="courses__box" id = "search">
               {result}
            </div>
            </React.Fragment> : <CreateCourse 
                authorList = {authors}
                coursesList = {courses}
                updateAuthor = {(newAuthors) => {
                    setAuthors([...authors, newAuthors])
                    console.log("all" + authors);
                }}
                create = {(newCourse) => {
                    setCourses([...courses, newCourse]);
                    setupdateRequest(courses);
                    console.log("all course" + courses);
                 }
             }
                onClick = {(event) => {
                event.preventDefault();
                setshowCourse(true);
                setshowaddCourse(false);
            }}/>}
            </div>
      );
  }
export default Courses;
  