import {React, useState} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css';

import Courses from './components/Courses/Courses';
import Header from './components/Header/Header';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import CourseInfo from './components/CourseInfo/CourseInfo';

import {mockedCoursesList, mockedAuthorsList, minInHour} from './constants'
import CreateCourse from './components/CreateCourse/CreateCourse';



function App() {
  const [courses, setCourses] = useState(mockedCoursesList);
  const [authors, setAuthors] = useState(mockedAuthorsList);


  function durationFormat(minutes) {
    let hours = (minutes / minInHour);
    let rhours = Math.floor(hours).toString();
    let min = (hours - rhours) * minInHour;
    let rminutes = Math.round(min).toString();
    let rezHour = '';
    let rezMin = '';
    if(rhours.length === 1) {
        rezHour = '0' + rhours;
    } else {
        rezHour = rhours;
    }
    if(rminutes.length === 1) {
        rezMin = '0' + rminutes;
    } else {
        rezMin = rminutes;
    }
    
    return rezHour + ':' + rezMin +  ' hours';
 }
 function dateFormat(date) {
     return date.split('/').join('.');
 }

function getAuthor(arrId) {
    let strAuthor = [];
    authors.forEach(element => {
       arrId.forEach(item => {
           if(element.id === item) {
               strAuthor.push(element.name);
           }
       })
    });
    return strAuthor.join(',');
}


  return (
    <Router>
       <Routes>
          <Route exact path='/' element = {<Header fetchStatus = 'home'/>}/>
          <Route exact path='/login' element = {<Header fetchStatus = 'auth'/>}/>
          <Route exact path='/register' element = {<Header fetchStatus = 'auth'/>}/>
          <Route exact path='/courses' element = {<Header fetchStatus = 'entered'/>}/>  
          <Route exact path='/courses/:courseId' element = {<Header fetchStatus = 'entered'/>}/>
        </Routes>
        <Routes>
          <Route exact path='/' element = {<Courses 
           courses = {courses} 
           authors ={authors}/>}/>
            <Route exact path='/courses' element = {<Courses 
           courses = {courses} 
           authors ={authors}/>}/>
          <Route exact path='/login' element = {<Login />}/>
          <Route exact path='/register' element = {<Registration />}/>
          <Route exact path='/courses/add' element = {<CreateCourse 
                durationFormat = {durationFormat}
                authorList = {authors}
                coursesList = {courses}
                updateAuthor = {(newAuthors) => {
                    setAuthors([...authors, newAuthors]);
                }}
                create = {(newCourse) => {
                    setCourses([...courses, newCourse]);
                 }
             }/>}/>
          <Route exact path='/courses/:courseId' element = {<CourseInfo 
          durationFormat = {durationFormat}
          dateFormat = {dateFormat}
          getAuthor = {getAuthor}
          courses = {courses} 
          authors ={authors}
          />}/>
        </Routes>
    </Router>
    );
}
export default App;
