import {React, useEffect, useState, useRef} from 'react';

import Button from "../../common/Button/Button";
import Logo from "./components/Logo";

import './Header.css';

import {buttonLogin, buttonSignUp, logout} from '../../constants'
import { Link, useNavigate, useParams } from 'react-router-dom';

function Header(props) {
 const navigate = useNavigate();
 const[user, setUser] = useState('');
 const[flag, setFlag] = useState(true);
 const[flagLogOut, setFlagLogOut] = useState(false);




 async function fetchData(){
   const response = await fetch('http://localhost:4000/users/me', {
     method: 'GET',
     headers: {
     'Authorization' : localStorage.getItem('token')
     },
    });
 const result = await response.json();
 if(result.successful === true) {
     setUser(result.result.name);
    }
 }

useEffect(() => {
    console.log(props.fetchStatus);
      if(props.fetchStatus === 'entered'){
        fetchData();
        setFlag(false);
        setFlagLogOut(true);
      } else if(props.fetchStatus === 'auth'){
        setFlag(false);
        setFlagLogOut(false);
      } else if(props.fetchStatus === 'home') {
        setFlag(true);
        setFlagLogOut(false);
      }
 }, [props.fetchStatus]);
 

  return (
      <header id = 'header'>
          <div className="header__wrapper">
           <div className="logo__wrapper">
               <Link to = {"/"}><Logo className = "logo"/></Link>
           </div>
           <div className="user__block">
               <p className="user__name">{user}</p>
               {flag && <> <Button buttonText = {buttonLogin}  onClick = {
                    (e) => {
                        e.preventDefault();
                        navigate("/login");
                        setUser('');
                        setFlag(false);
                        setFlagLogOut(false);
                    }
                }/>
               <Button buttonText = {buttonSignUp} onClick = {
                    (e) => {
                        e.preventDefault();
                        navigate("/register");
                        setUser('');
                        setFlag(false);
                        setFlagLogOut(false);
                    }
                }/> </>}
                {flagLogOut && <Button buttonText = {logout} onClick = {
                    (e) => {
                        e.preventDefault();
                        navigate("/login");
                        localStorage.removeItem('token');
                        setUser('');
                        setFlag(false);
                        setFlagLogOut(false);
                    }
                }/>}
           </div>
          </div>          
      </header>
    );
}
export default Header;
