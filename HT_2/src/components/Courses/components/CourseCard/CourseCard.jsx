import React from 'react';

import Button from '../../../../common/Button/Button';

import {cardBtn, minInHour} from '../../../../constants'

import './CourseCard.css';

function СourseCard(props) {
const authorsList = props.authorsList;

function durationFormat(minutes) {
    let hours = (minutes / minInHour);
    let rhours = Math.floor(hours).toString();
    let min = (hours - rhours) * minInHour;
    let rminutes = Math.round(min).toString();
    let rezHour = '';
    let rezMin = '';
    if(rhours.length === 1) {
        rezHour = '0' + rhours;
    } else {
        rezHour = rhours;
    }
    if(rminutes.length === 1) {
        rezMin = '0' + rminutes;
    } else {
        rezMin = rminutes;
    }
    
    return rezHour + ':' + rezMin +  ' hours';
 }
 function dateFormat(date) {
     return date.split('/').join('.');
 }
 function getAuthor(arrId) {
     let strAuthor = [];
     authorsList.forEach(element => {
        arrId.forEach(item => {
            if(element.id === item) {
                strAuthor.push(element.name);
            }
        })
     });
     return strAuthor.join(',');
 }
  return (
      <section className = "card">
          <div className="card__wrapper">
              <div className="main__info"> 
              <h3 className="card__title">{props.title}</h3>
              <hr/>
              <p className="card__par">{props.description}</p>
              </div>
              <div className="add_info">
                  <div className="nowrap add__row"><span>Authors:  </span>{getAuthor(props.authors)}</div>
                  <div className="add__row"><span>Duration:  </span>{durationFormat(props.duration)}</div>
                  <div className="add__row"><span>Created:  </span>{dateFormat(props.creationDate)}</div>
                  <Button buttonText = {cardBtn} onClick = {props.onClick}/>
              </div>
          </div>
      </section>
    );
}
export default СourseCard;
