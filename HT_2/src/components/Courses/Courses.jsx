import Button from "../../common/Button/Button";
import SearchBar from "./components/SearchBar/SearchBar";
import CourseCard from './components/CourseCard/CourseCard';


import {btnAddNew, title} from '../../constants';

import './Courses.css';

import React, {useState, useEffect} from "react";
import { useNavigate  } from 'react-router-dom';


function Courses(props) {

    const [searchTerm, setSearchTerm] = useState('');
    const [request, setRequest] = useState();
    const [result, setResult] = useState();

    const courses = props.courses;
    const authors = props.authors;


    const navigate = useNavigate();


    
    useEffect(() => {
        const rez = courses.filter((obj) => {
            if(searchTerm === ''){
                return obj;
            } else if(obj.title.toLowerCase().includes(searchTerm.toLowerCase())){
                return obj;
            } else if(obj.id.toLowerCase().includes(searchTerm.toLowerCase())) {
                return obj;
            }
            return 0;
          }).map((obj) =>
          <CourseCard key={obj.id} 
          authorsList = {authors}
          title = {obj.title} 
          description = {obj.description} 
          authors = {obj.authors}
          duration = {obj.duration}
          creationDate = {obj.creationDate}
          onClick = {(e) => {
            e.preventDefault();
            navigate(`/courses/${obj.id}`);
          }}/>);
        setResult(rez);
    },[searchTerm, request, courses, authors, navigate])

    
    return (
        <div className = "user__section">
                <div className="user__section_row">
                <SearchBar 
                onChange = {(event) => {
                    setSearchTerm(event.target.value);
                }}
                onClick = {() => setRequest(searchTerm)}
                labelText = {title}/>
                <Button buttonText = {btnAddNew} 
                onClick = {() => {
                    navigate("/courses/add");
                }}
                />
            </div>
            <div className="courses__box" id = "search">
               {result}
            </div>
            </div>
      );
  }
export default Courses;
  