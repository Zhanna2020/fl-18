import {React, useState, useEffect} from 'react';

import './Login.css';

import {register, registerLabel, login} from '../../constants'
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { Link, useNavigate  } from 'react-router-dom';


function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [dirtyEmail, setdirtyEmail] = useState(false);
    const [dirtyPassword, setdirtyPassword] = useState(false);

    const [dirtyEmailError, setdirtyEmailError] = useState('Input can not be empty');
    const [dirtyPasswordError, setdirtyPasswordError] = useState('Input can not be empty');

    useEffect(() => {
        localStorage.removeItem('token');
    });
    const navigate = useNavigate();

    const blurHandler = (e) => {
        switch (e.target.name) {
        case 'email':
            setdirtyEmail(true);
            break;
        case 'password':
            setdirtyPassword(true);
            break;
        default:
            break;
        }
    }
    const validation = (e) => {
        switch (e.target.name) {
            case 'email':
                if(!(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email))){
                    setdirtyEmailError('Invalid email');
                    setdirtyEmail(true);
                } else {
                    setdirtyEmailError('');
                    setdirtyEmail(false);
                }
                break;
            case 'password':
                if(password.length <= 4 || !(/(?=.*[0-9])(?=.*[a-z]){6,}/g.test(password))) {
                    setdirtyPasswordError('Invalid password');
                    setdirtyPassword(true);
                } else {
                    setdirtyPasswordError('');
                    setdirtyPassword(false);
                }
                break;
            default:
                break;
    }
}
    async function handleSubmit(e) {
        e.preventDefault();
        if(dirtyEmailError !== ''){
            alert('Enter correct email')
        } else if(dirtyPasswordError !== ''){
            alert('Enter correct password')
        } else {
            const loginUser = {
                email: email,
                password: password
            };
            const response = await fetch('http://localhost:4000/login', {
                method: 'POST',
                body: JSON.stringify(loginUser),
                headers: {
                'Content-Type': 'application/json',
                },
               });
            const result = await response.json();
            if(result.successful === true) {
                localStorage.setItem('token', result.result);
                navigate('/courses');  
            } else {
                alert('Error of authonification')
            }
        }
    }
  return (
      <form className='login' onSubmit={handleSubmit}>
          <div className='login__wrapper'>
              <h2 className='login__title'>Login</h2>
                {(dirtyEmail && dirtyEmailError) &&     
               <div className="error">{dirtyEmailError}</div>}
                <Input placeholdetText = {register.email} 
                    onChange = {(e) => {
                        setEmail(e.target.value);
                        validation(e);
                    }} 
                    labelText={registerLabel.email}
                    name = 'email'
                    value={email}
                    type = 'email'
                    onBlur = {e => blurHandler(e)}/>
                {(dirtyPassword && dirtyPasswordError) &&     
               <div className="error">{dirtyPasswordError}</div>}
                <Input placeholdetText = {register.password} 
                    onChange = {(e) => {
                        setPassword(e.target.value);
                        validation(e);
                    }} 
                    labelText={registerLabel.password}
                    name = 'password'
                    value={password}
                    type = 'password'
                    onBlur = {e => blurHandler(e)}/>
                <Button buttonText = {login} 
                    type = "submit"/>
                <div>
                    If you don't have an account, you can <Link to={"/register"}>Register</Link>
                </div>
          </div>
      </form>
    );
}
export default Login;
