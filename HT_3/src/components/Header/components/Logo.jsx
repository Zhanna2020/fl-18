import LogoPic from './assets/online-learning.png'

import './Logo.css';

import {altForLogo} from "../../../constants"

function Logo() {
    return (
        <img 
        src={LogoPic}
        alt={altForLogo}/>
      );
  }
export default Logo;
  