import {React, useEffect, useState} from 'react';

import Button from "../../common/Button/Button";
import Logo from "./components/Logo";

import './Header.css';

import {buttonLogin, buttonSignUp, logout} from '../../constants'
import { Link, useNavigate } from 'react-router-dom';

import { useDispatch, useSelector } from "react-redux";
import {getUser} from '../../store/selectors';
import {setUserAction} from '../../store/user/actionCreators'

function Header(props) {
 const dispatch = useDispatch();
 const user = useSelector(getUser);

 const navigate = useNavigate();
 const[flag, setFlag] = useState(true);
 const[flagLogOut, setFlagLogOut] = useState(false);



useEffect(() => {
    console.log(props.fetchStatus);
      if(props.fetchStatus === 'entered'){
        setFlag(false);
        setFlagLogOut(true);
      } else if(props.fetchStatus === 'auth'){
        setFlag(false);
        setFlagLogOut(false);
      } else if(props.fetchStatus === 'home') {
        setFlag(true);
        setFlagLogOut(false);
      }
 }, [props.fetchStatus]);
 

  return (
      <header id = 'header'>
          <div className="header__wrapper">
           <div className="logo__wrapper">
               <Link to = {"/"}><Logo className = "logo"/></Link>
           </div>
           <div className="user__block">
               <p className="user__name">{user.name}</p>
               {flag && <> <Button buttonText = {buttonLogin}  onClick = {
                    (e) => {
                        e.preventDefault();
                        navigate("/login");
                        setFlag(false);
                        setFlagLogOut(false);
                    }
                }/>
               <Button buttonText = {buttonSignUp} onClick = {
                    (e) => {
                        e.preventDefault();
                        navigate("/register");
                        setFlag(false);
                        setFlagLogOut(false);
                    }
                }/> </>}
                {flagLogOut && <Button buttonText = {logout} onClick = {
                    (e) => {
                        e.preventDefault();
                        navigate("/login");
                        localStorage.removeItem('token');
                        const userResponse = {
                          isAuth: false,
                          name: '',
                          email: '',
                          token: ''
                      };
                      dispatch(setUserAction(userResponse));
                      console.log(user);
                        setFlag(false);
                        setFlagLogOut(false);
                    }
                }/>}
           </div>
          </div>          
      </header>
    );
}
export default Header;
