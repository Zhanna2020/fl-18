import {SET_USER} from './actionTypes';

const userInitialState = {
    isAuth: false,
    name: '',
    email: '',
    token: ''
}

export const userReducer = (state = userInitialState, action) =>  {
    switch (action.type) {
        case SET_USER:
            return {...state, 
            isAuth: action.payload.isAuth,
            name: action.payload.name,
            email: action.payload.email,
            token: action.payload.token,};
        default:
            return state;
    }
};