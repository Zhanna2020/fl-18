import {getCoursesAction} from '../store/courses/actionCreators';
import {getAuthorsAction} from '../store/authors/actionCreators'


export const fetchCourses = () => {
    return async function(dispatch) {
    const response = await fetch('http://localhost:4000/courses/all', {
     method: 'GET',
     headers: {
        'Content-Type': 'application/json',
    }
    });
    const result = await response.json();
    dispatch(getCoursesAction(result.result));
  }
}
export const fetchAuthors = () => {
  return async function(dispatch) {
  const response = await fetch('http://localhost:4000/authors/all', {
   method: 'GET',
   headers: {
      'Content-Type': 'application/json',
  }
  });
  const result = await response.json();
  dispatch(getAuthorsAction(result.result));
 }
}

